# Run SimPy benchmarks.
#
# Usage:
#
# python benchmark.py
#     Run selected benchmarks for the tip revision.
#
# python benchmark.py 2vs3
#     Run selected benchmarks for SimPy 2.3 and the tip revision.
#
# python benchmark.py [REV+]
#     Run selected benchmarks for all specified revisions.
#
#
# Config:
repo = '/Users/stefan/Code/simpy'
benchmarks = [
    ('resource',),
    ('simple_sim', '316, 316'),
    ('simple_sim', '10, 10000'),
    ('simple_sim', '10000, 10'),
    ('wait_for_proc', '100, 100'),
    ('subscribe', '100, 100'),
    ('condition_events', '10000'),
    ('condition_wait', '10000'),
]
num_runs = 20
profile = False

# Don't change anything from here.
import cProfile
import multiprocessing
import subprocess
import sys
import timeit


def run_benchmark(simpy, rev, module, *args):
    """Run one benchmark module and print its results."""
    print('Running %s[%s].%s(%s) ...' % (simpy, rev, module, ', '.join(args)))
    if rev:
        subprocess.call('hg up %s -R %s' % (rev, repo), stdout=subprocess.PIPE,
                        shell=True)
    res = timeit.repeat('benchmark(%s)' % ', '.join(args),
                        'from %s.%s import benchmark' % (simpy, module),
                        repeat=num_runs, number=1)
    print('... %.2fs' % min(res))

    if profile:
        cProfile.run('from %s.%s import benchmark; benchmark(%s)' %
                     (simpy, module, args))


def run_benchmarks(simpy, rev):
    """Run all benchmarks for revision ``rev.``."""
    sys.path.insert(0, repo)
    print('Running benchmarks for rev. %s ...' % rev)
    for benchmark in benchmarks:
        proc = multiprocessing.Process(target=run_benchmark,
                args=(simpy, rev,) + benchmark)
        proc.start()
        proc.join()


def main():
    print('Best of %s runs:' % num_runs)
    if len(sys.argv) == 1:
        run_benchmarks('simpy3', None)

    elif sys.argv[1] == '2vs3':
        run_benchmarks('simpy2', '2.3.1')
        run_benchmarks('simpy3', 'tip')

    else:
        for rev in sys.argv[1:]:
            run_benchmarks('simpy3', rev)


if __name__ == '__main__':
    main()
