from SimPy.Simulation import Simulation, Process, hold


class Proc(Process):
    def pem(self):
        while True:
            yield hold, self, 1


def benchmark(procs=100, until=100):
    sim = Simulation()
    for i in range(procs):
        p = Proc(sim=sim)
        sim.activate(p, p.pem())
    sim.simulate(until=until)
