import simpy


def proc(env):
    yield (env.timeout(0) & (env.timeout(2) | env.timeout(1)))


def benchmark(procs):
    env = simpy.Environment()
    for i in range(procs):
        env.process(proc(env))
    env.run()
