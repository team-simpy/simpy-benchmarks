import random

import simpy


def child(env):
    yield env.timeout(random.randint(1, 1000))


def parent(env, num_childs):
    childs = [env.process(child(env)) for i in range(num_childs)]

    for child_proc in childs:
        if not child_proc.triggered:
            yield child_proc


def benchmark(num_parents=100, num_childs=1000):
    env = simpy.Environment()
    for i in range(num_parents):
        env.process(parent(env, num_childs))
    env.run()
