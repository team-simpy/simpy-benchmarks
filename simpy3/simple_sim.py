from simpy import Environment


def pem(env):
    while True:
        yield env.timeout(1)


def benchmark(procs=100, until=100):
    env = Environment()
    for i in range(procs):
        env.process(pem(env))
    env.run(until)
