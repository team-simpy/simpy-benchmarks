import simpy
import simpy.util


def proc(env):
    yield env.all_of([env.timeout(i) for i in range(10)])


def benchmark(procs):
    env = simpy.Environment()
    for i in range(procs):
        env.process(proc(env))
    env.run()

