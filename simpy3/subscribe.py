import random

from simpy.util import subscribe_at
import simpy



def child(env):
    yield env.timeout(random.randint(1, 1000))


def parent(env, num_childs):
    childs = []
    for i in range(num_childs):
        child_proc = env.process(child(env))
        subscribe_at(child_proc)
        childs.append(child_proc)

    for i in range(num_childs):
        try:
            yield env.event()
        except simpy.Interrupt:
            pass


def benchmark(num_parents=100, num_childs=1000):
    env = simpy.Environment()
    for i in range(num_parents):
        env.process(parent(env, num_childs))
    env.run()
