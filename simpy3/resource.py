from simpy import Environment
from simpy.resources.resource import Resource


def pem(env, resource):
    while True:
        with resource.request() as req:
            yield req
            yield env.timeout(1)


def benchmark(procs=2000, capacity=100, until=100):
    env = Environment()
    resource = Resource(env, capacity)
    for i in range(procs):
        env.process(pem(env, resource))
    env.run(until)

